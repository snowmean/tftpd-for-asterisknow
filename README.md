Micro TFTP server implementation on Ruby.
-----------------------------------------
Help to automatically set up CISCO and Linksys SIP phones, using existing AsteriskNOW configuration.

HOWTO
-----
1. Set up your DHCP server /etc/dhcp/dhcpd.conf like this

		ddns-update-style interim;
		ignore client-updates;
		option tftp150 code 150 = string;
		option tftp66 code 66 = string;
		DHCPARGS=eth0;

		subnet 192.168.0.1 netmask 255.255.255.0 {
		    option routers 192.168.0.1;
		    option subnet-mask 255.255.255.0;
		    option domain-name "domain.local";
		    option domain-name-servers 192.168.0.1;
		    option tftp-server-name "192.168.0.1";
		    option tftp150 "192.168.0.1";
		    option tftp66 "192.168.0.1";
		    option ntp-servers 192.168.0.1;
		    range dynamic-bootp 192.168.0.10 192.168.0.200;
		    default-lease-time 43200;
		    max-lease-time 86400;
		}

1. Add extensions into AsteriskNOW using FreePBX web GUI, putting phone's MAC address as a secret.
For some reasons, if MAC address includes less than 2 letters, you may add "xx" at the end of MAC.

1. Change SIP and NTP servers IP into xml.cfg.erb to yours

1. Run tftpd.rb

1. Make Factory Reset on your device, and wait until it boot and then reboot.
After that your device will connect to your Asterisk server automatically.
