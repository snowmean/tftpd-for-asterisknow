#!/usr/bin/ruby
#encoding: utf-8

require 'rubygems'
require 'socket'
require 'configparser.rb'
require 'yaml'
require 'erb'
require 'stringio'
require 'logger'

path = "/opt/tftp"
logger = Logger.new "/var/log/tftpd.log"

class TFTPOpCode
  RRQ = 1
  WRQ = 2
  DATA = 3
  ACK = 4
  ERROR = 5
  OACK = 6
end

class TFTPImpl

	attr_accessor :socket, :client_ip, :client_mac, :client_port

	def initialize
		@socket = UDPSocket.new
		@socket.bind "", 69
	end

	def recv
		msg, client = @socket.recvfrom 65535
		code, file_name = msg.unpack "nZ*"
		@client_ip = client[2]
		@client_port = client[1]
		@client_mac = self.get_mac
		return {"code" => code, "file_name" => file_name}
	end

	def send(xml_data, blocksize=512)
		writen = 0
		blocknum = 0
		data = StringIO.new(xml_data)
		while writen < data.size
			blocknum = (blocknum + 1) % 65536
			buf = data.read(blocksize)
			@socket.send([TFTPOpCode::DATA, blocknum, buf].pack("nna*"), 0, @client_ip, @client_port)
			
			# ACK Check
			msg, client = @socket.recvfrom(65535)
			redo if msg.unpack("n")[0] == TFTPOpCode::ERROR
			
			writen += blocksize
		end
	end

	def get_mac
		return `arping -w 1 -f #{@client_ip} | awk '/Unicast reply from/ {print $5;}' | tr -d [:\[\]]`.chomp
		#return "000E08D10C50"
	end
end

@tftp = TFTPImpl.new

while true
	conn = @tftp.recv

	if conn['code'] != TFTPOpCode::RRQ
		logger.warn "IP #{@tftp.client_ip} sent non RRQ request: code = #{conn['code']}"
		redo
	end

	@sip = ConfigParser.new "/etc/asterisk/sip_additional.conf"

	logger.info "Connection from #{@tftp.client_ip} established."
	user_id = @sip.select { |key, hash| hash['secret'].upcase == @tftp.client_mac or hash['secret'].upcase == "#{@tftp.client_mac}XX" }[0]
	if not user_id.nil?
		user_id = user_id[0]
		if @sip.include? user_id
			user_passwd = @sip[user_id]["secret"]
			user_name = @sip[user_id]["callerid"].sub(" <#{user_id}>", "")
			xml = ERB.new(File.open("#{path}/xml.cfg.erb", "r").read).result
			nat = @sip[user_id]["nat"]
			logger.info "Asked for #{conn['file_name']} sending configuration for #{user_id}"
			@tftp.send(xml)
		else
			logger.error "SIP server have no phone number for MAC: #{@tftp.client_mac}"
		end
	else
		logger.error "Unknown client with MAC: #{@tftp.client_mac}"
	end
end
